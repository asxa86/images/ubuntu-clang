FROM ubuntu:23.10

COPY clang/ /usr/

RUN apt update -y && \
    apt install -y g++-13 && \
    update-alternatives --install /usr/bin/cc cc /usr/bin/clang 20 && \
    update-alternatives --set cc /usr/bin/clang && \
    update-alternatives --install /usr/bin/c++ c++ /usr/bin/clang++ 20 && \
    update-alternatives --set c++ /usr/bin/clang++
